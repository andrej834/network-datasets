import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.TreeSet;
 
public class Amalfi {
 
    public static int calc(TreeMap<Integer, TreeSet<Integer>> tm) {      
        int sum = 2;
       
        for (Integer i : tm.keySet()) {
            sum+=tm.get(i).size()*2+2;
        }
       
        return sum;  
    }
   
    public static void convert_to_LE(int val, int index) {
        byte b1 = (byte)((val & 0x0000FF00)>>8); //zgornji
        byte b2 = (byte)(val & 0x000000FF); //spodnji
       
        output[index]=b2;
        output[index+1]=b1;
    }
   
    public static TreeMap<Integer, Integer> preslikave;
   
    public static void preslikava(TreeMap<Integer, TreeSet<Integer>> tm) {
        preslikave = new TreeMap<>();
        int number = 0;
        for (Integer integer : tm.keySet()) {
            preslikave.put(integer, number);
            number++;
        }
    }
   
    public static void createA_List(String name) throws IOException {
        TreeMap<Integer, TreeSet<Integer>> tm = new TreeMap<>();
       
        //preberemo file in naredimo svoj magic -> odstranimo vse duplikate ter ustvarimo povratne povezave
        //dodamo tudi prazne node po potrebi
       
        while(vhod.hasNextLine()) {
           
            String s = vhod.nextLine();
            String line[];
           
            if(s.contains(",")) {
                line=s.split(",");
            }else {
                line=s.split(" ");
            }
       
            int k = Integer.parseInt(line[0]);
            int v = Integer.parseInt(line[1]);
           
            if(tm.containsKey(k)) {
                TreeSet<Integer> temp = tm.get(k);
                temp.add(v);
                tm.put(k, temp);
                if(tm.containsKey(v)) {
                    TreeSet<Integer> temp1 = tm.get(v);
                    temp1.add(k);
                    tm.put(v, temp1);
                }else {
                    TreeSet<Integer> temp1 = new TreeSet<>();
                    temp1.add(k);
                    tm.put(v, temp1);
                }
            }else {
                TreeSet<Integer> temp = new TreeSet<>();
                temp.add(v);
                tm.put(k, temp);
                if(tm.containsKey(v)) {
                    TreeSet<Integer> temp1 = tm.get(v);
                    temp1.add(k);
                    tm.put(v, temp1);
                }else {
                    TreeSet<Integer> temp1 = new TreeSet<>();
                    temp1.add(k);
                    tm.put(v, temp1);
                }
            }
        }
       
        vhod.close();
        preslikava(tm);
        TreeMap<Integer, TreeSet<Integer>> newTm = new TreeMap<>();
       
        for (Integer val : tm.keySet()) {
            TreeSet<Integer> tempo = new TreeSet<>();
            TreeSet<Integer> main = tm.get(val);
            if(main!=null) {
                for (Integer integer : main) {
                    int p = preslikave.get(integer);
                    tempo.add(p);
                }
                newTm.put(preslikave.get(val), tempo);
            }
        }
       
        //izracunamo size nasega byte arraya
        int size = calc(newTm);
        output = new byte[size];
        int index = 0;
 
        //dodamo st nodov na prvo mesto
        convert_to_LE(newTm.size(), 0);
        index+=2;
        //gremo cez vse node in njihove povezave, ter jih shranimo v nas byte array
        for (Integer i : newTm.keySet()) {
            TreeSet<Integer> temp = newTm.get(i);
            //dodamo stevilo povezav, ki jih trenutni node ima
            convert_to_LE(temp.size(),index);
            index+=2;
            //if(temp.size()>0) {
                //sprehod cez vse nase povezave trenutnega noda
                for (Integer val : temp) {
                    convert_to_LE(val, index);
                    index+=2;
                }
            //}
        }
        //stvar zapisemo v file
        FileOutputStream stream = new FileOutputStream(name+".bin");
        try {
            stream.write(output);
        } catch (IOException e) {
            e.printStackTrace();
        }
        stream.close();
    }
   
    public static Scanner vhod;
    public static byte[] output;
   
    public static void main(String[] args) throws IOException {
 
        File dir = new File("C:\\Users\\Administrator\\Desktop\\Network datasets\\RAW\\Brain\\Bla");
        int i = 1;
       
        //gremo cez vse file v trenutnem imeniku
        for (final File f : dir.listFiles()) {
            if(!f.isDirectory()) {
                System.out.println("WORKING ON "+f.getName()+" "+i++);
                vhod = new Scanner(f);
                if(f.getName().contains(".edges")) {
                    createA_List(f.getName().replaceAll(".edges", ""));
                }else if(f.getName().contains(".mtx")) {
                    //preskoci prve 2 vrstici, ker v mtx formatu tam ni pomembnih podatkov
                    vhod.nextLine();
                    vhod.nextLine();
                    createA_List(f.getName().replaceAll(".mtx", ""));
                }
            }
        }
        System.out.println("DONE");
    }
}