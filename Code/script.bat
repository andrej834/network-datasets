@echo off
IF "%1"=="" GOTO Run
 echo. & echo Checking all URLs in urls.txt & echo After finishing a log.txt will be created & echo. 

 @echo URL [Status] : > log.txt
 for /f "tokens=1,2 delims=;" %%a in (%1) do (
  echo [CHECKING]: %%a & echo [ON]: %%b
  ping -n 1 %%a | find "Reply" > NUL
  if not errorlevel 1 (echo %%b [OK] >> log.txt) else echo %%b [ERROR] >> log.txt
 )
 
echo. & echo Finished. Terminating batch file ...  & echo. & pause
start "" "log.txt" & exit  
:Run
IF "%2"=="run" GOTO Finish
call script.bat urls.txt "run" & exit
:Finish
exit