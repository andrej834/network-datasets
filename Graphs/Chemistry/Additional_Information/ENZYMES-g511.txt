Nodes	30
Edges	134
Density	0.308
Maximum degree	7
Minimum degree	0
Average degree	4.47
Assortativity	-0.03
Number of triangles	101
Average number of triangles	3.37
Maximum number of triangles	8
Average clustering coefficient	0.475
Fraction of closed triangles	0.421
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	5

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g511.php