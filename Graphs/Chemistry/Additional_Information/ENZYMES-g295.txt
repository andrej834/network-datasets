Nodes	123
Edges	278
Density	0.0371
Maximum degree	5
Minimum degree	0
Average degree	2.26
Assortativity	-0
Number of triangles	6
Average number of triangles	0.05
Maximum number of triangles	2
Average clustering coefficient	0.006
Fraction of closed triangles	0.029
Maximum k-core	2
Maximum k-truss	1
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g295.php