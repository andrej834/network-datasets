Nodes	21
Edges	100
Density	0.4762
Maximum degree	6
Minimum degree	0
Average degree	4.76
Assortativity	-0.05
Number of triangles	117
Average number of triangles	5.57
Maximum number of triangles	10
Average clustering coefficient	0.627
Fraction of closed triangles	0.606
Maximum k-core	4
Maximum k-truss	3
Upper bound of Chromatic number	5

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g58.php