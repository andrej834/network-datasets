Nodes	11
Edges	38
Density	0.6909
Maximum degree	5
Minimum degree	0
Average degree	3.45
Assortativity	-0.14
Number of triangles	10
Average number of triangles	0.91
Maximum number of triangles	2
Average clustering coefficient	0.314
Fraction of closed triangles	0.238
Maximum k-core	2
Maximum k-truss	1
Upper bound of Chromatic number	3

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g368.php