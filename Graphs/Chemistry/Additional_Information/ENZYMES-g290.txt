Nodes	38
Edges	148
Density	0.2105
Maximum degree	6
Minimum degree	0
Average degree	3.89
Assortativity	-0.03
Number of triangles	130
Average number of triangles	3.42
Maximum number of triangles	6.5
Average clustering coefficient	0.624
Fraction of closed triangles	0.575
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	5

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g290.php