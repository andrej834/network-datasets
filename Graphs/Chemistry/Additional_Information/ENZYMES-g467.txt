Nodes	20
Edges	82
Density	0.4316
Maximum degree	6
Minimum degree	0
Average degree	4.1
Assortativity	-0.02
Number of triangles	44
Average number of triangles	2.2
Maximum number of triangles	5
Average clustering coefficient	0.308
Fraction of closed triangles	0.331
Maximum k-core	3
Maximum k-truss	1
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g467.php