Nodes	95
Edges	242
Density	0.0542
Maximum degree	5
Minimum degree	0
Average degree	2.55
Assortativity	0.01
Number of triangles	0
Average number of triangles	0
Maximum number of triangles	0
Average clustering coefficient	0
Fraction of closed triangles	0
Maximum k-core	2
Maximum k-truss	0
Upper bound of Chromatic number	3

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g118.php