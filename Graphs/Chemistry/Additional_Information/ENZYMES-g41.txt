Nodes	47
Edges	190
Density	0.1758
Maximum degree	7
Minimum degree	0
Average degree	4.04
Assortativity	-0.01
Number of triangles	110
Average number of triangles	2.34
Maximum number of triangles	5
Average clustering coefficient	0.387
Fraction of closed triangles	0.356
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	5

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g41.php