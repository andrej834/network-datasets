Nodes	30
Edges	120
Density	0.2759
Maximum degree	6
Minimum degree	0
Average degree	4
Assortativity	-0.06
Number of triangles	89
Average number of triangles	2.97
Maximum number of triangles	7.5
Average clustering coefficient	0.48
Fraction of closed triangles	0.478
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g193.php