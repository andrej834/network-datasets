Nodes	13
Edges	52
Density	0.6667
Maximum degree	7
Minimum degree	0
Average degree	4
Assortativity	-0.09
Number of triangles	45
Average number of triangles	3.46
Maximum number of triangles	7
Average clustering coefficient	0.577
Fraction of closed triangles	0.542
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	5

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g135.php