Nodes	41
Edges	172
Density	0.2098
Maximum degree	6
Minimum degree	0
Average degree	4.2
Assortativity	-0.04
Number of triangles	84
Average number of triangles	2.05
Maximum number of triangles	4.5
Average clustering coefficient	0.324
Fraction of closed triangles	0.3
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g182.php