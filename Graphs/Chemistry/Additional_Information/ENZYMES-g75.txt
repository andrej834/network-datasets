Nodes	20
Edges	72
Density	0.3789
Maximum degree	6
Minimum degree	0
Average degree	3.6
Assortativity	-0.09
Number of triangles	53
Average number of triangles	2.65
Maximum number of triangles	6.5
Average clustering coefficient	0.599
Fraction of closed triangles	0.558
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g75.php