Nodes	19
Edges	68
Density	0.3977
Maximum degree	5
Minimum degree	0
Average degree	3.58
Assortativity	-0.04
Number of triangles	51
Average number of triangles	2.68
Maximum number of triangles	5
Average clustering coefficient	0.595
Fraction of closed triangles	0.567
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	4

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g565.php