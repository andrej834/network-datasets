Nodes	19
Edges	78
Density	0.4561
Maximum degree	6
Minimum degree	0
Average degree	4.11
Assortativity	-0.01
Number of triangles	55
Average number of triangles	2.89
Maximum number of triangles	6
Average clustering coefficient	0.436
Fraction of closed triangles	0.43
Maximum k-core	3
Maximum k-truss	2
Upper bound of Chromatic number	5

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g397.php