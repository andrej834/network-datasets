Nodes	96
Edges	218
Density	0.0478
Maximum degree	6
Minimum degree	0
Average degree	2.27
Assortativity	-0.03
Number of triangles	12
Average number of triangles	0.13
Maximum number of triangles	2
Average clustering coefficient	0.049
Fraction of closed triangles	0.077
Maximum k-core	2
Maximum k-truss	1
Upper bound of Chromatic number	3

Category	Sparse networks
Collection	Chemistry networks
Tags Chemistry, Chemistry networks
Short	Chemistry networks
Edge weights	Unweighted

[LOCATION]: http://networkrepository.com/ENZYMES-g293.php