Nodes	54
Edges	353
Density	0.246681
Maximum degree	48
Minimum degree	4
Average degree	13
Assortativity	-0.227075
Number of triangles	2K
Average number of triangles	36
Maximum number of triangles	216
Average clustering coefficient	0.413868
Fraction of closed triangles	0.334174
Maximum k-core	11
Lower bound of Maximum Clique	6

Category Sparse networks
Collection Ecology networks
Tags Ecology, Ecology networks
Edge weights Unweighted

[LOCATION]: http://networkrepository.com/eco-stmarks.php