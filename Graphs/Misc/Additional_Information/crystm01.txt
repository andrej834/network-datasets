Nodes 4875
Edges 50232
Maximum degree 26
Average degree 20
Assortativity 0.300091
Number of triangles 471744
Average number of triangles 96
Maximum number of triangles 132
Average clustering coefficient 0.490522
Fraction of closed triangles 0.447308
Maximum k-core 13
Lower bound of Maximum Clique 8
