Nodes 1000
Edges 9990
Maximum degree 36
Average degree 19
Assortativity 0.00833592
Number of triangles 4176
Average number of triangles 4
Maximum number of triangles 16
Average clustering coefficient 0.0208657
Fraction of closed triangles 0.0209432
Maximum k-core 15
Lower bound of Maximum Clique 3
