Nodes 2600
Edges 35880
Maximum degree 30
Average degree 27
Assortativity 0.265069
Number of triangles 107640
Average number of triangles 41
Maximum number of triangles 45
Average clustering coefficient 0.113211
Fraction of closed triangles 0.112378
Maximum k-core 25
Lower bound of Maximum Clique 4
