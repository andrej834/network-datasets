Nodes 3160
Edges 29816
Maximum degree 54
Average degree 18
Assortativity 0.0406713
Number of triangles 8552
Average number of triangles 2
Maximum number of triangles 43
Average clustering coefficient 0.0164227
Fraction of closed triangles 0.0142883
Maximum k-core 13
Lower bound of Maximum Clique 4
