Nodes 2398
Edges 61432
Maximum degree 120
Average degree 51
Assortativity -0.151064
Number of triangles 1119447
Average number of triangles 466
Maximum number of triangles 2422
Average clustering coefficient 0.354305
Fraction of closed triangles 0.307168
Maximum k-core 45
Lower bound of Maximum Clique 23
