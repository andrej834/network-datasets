Nodes 9000
Edges 1635345
Maximum degree 514
Average degree 363
Assortativity 0.487999
Number of triangles 312219840
Average number of triangles 34691
Maximum number of triangles 58117
Average clustering coefficient 0.528248
Fraction of closed triangles 0.49318
Maximum k-core 201
Lower bound of Maximum Clique 57
