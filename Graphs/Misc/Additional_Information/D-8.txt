Nodes 1270
Edges 14943
Maximum degree 42
Average degree 23
Assortativity 0.122873
Number of triangles 8211
Average number of triangles 6
Maximum number of triangles 34
Average clustering coefficient 0.0223867
Fraction of closed triangles 0.0224134
Maximum k-core 17
Lower bound of Maximum Clique 4
