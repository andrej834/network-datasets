Nodes 172
Edges 649
Maximum degree 44
Average degree 7
Assortativity -0.650431
Number of triangles 726
Average number of triangles 4
Maximum number of triangles 58
Average clustering coefficient 0.205384
Fraction of closed triangles 0.065636
Maximum k-core 5
Lower bound of Maximum Clique 5
