Nodes 636
Edges 5365
Maximum degree 41
Average degree 16
Assortativity 0.078314
Number of triangles 3708
Average number of triangles 5
Maximum number of triangles 31
Average clustering coefficient 0.0328656
Fraction of closed triangles 0.036419
Maximum k-core 13
Lower bound of Maximum Clique 4
