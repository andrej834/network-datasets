Nodes 946
Edges 8232
Maximum degree 42
Average degree 17
Assortativity -0.0410395
Number of triangles 9919
Average number of triangles 10
Maximum number of triangles 550
Average clustering coefficient 0.0224467
Fraction of closed triangles 0.0475699
Maximum k-core 28
Lower bound of Maximum Clique 16
