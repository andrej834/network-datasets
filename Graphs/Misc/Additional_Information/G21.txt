Nodes 800
Edges 4667
Maximum degree 143
Average degree 11
Assortativity -0.0611184
Number of triangles 16155
Average number of triangles 20
Maximum number of triangles 477
Average clustering coefficient 0.349631
Fraction of closed triangles 0.153391
Maximum k-core 7
Lower bound of Maximum Clique 6
