Nodes 4562
Edges 134824
Maximum degree 122
Average degree 59
Assortativity -0.084481
Number of triangles 5382824
Average number of triangles 1179
Maximum number of triangles 2832
Average clustering coefficient 0.784387
Fraction of closed triangles 0.536704
Maximum k-core 37
Lower bound of Maximum Clique 19
