Nodes 4098
Edges 34129
Maximum degree 783
Average degree 16
Assortativity -0.0291766
Number of triangles 298821
Average number of triangles 72
Maximum number of triangles 4259
Average clustering coefficient 0.428212
Fraction of closed triangles 0.228481
Maximum k-core 27
Lower bound of Maximum Clique 16
