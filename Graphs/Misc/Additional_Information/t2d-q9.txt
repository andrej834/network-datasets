Nodes 9801
Edges 77224
Maximum degree 16
Average degree 15
Assortativity 0.391415
Number of triangles 460992
Average number of triangles 47
Maximum number of triangles 48
Average clustering coefficient 0.405442
Fraction of closed triangles 0.402051
Maximum k-core 9
Lower bound of Maximum Clique 4
