Nodes 324
Edges 13203
Maximum degree 82
Average degree 81
Assortativity 0.975459
Number of triangles 1023840
Average number of triangles 3160
Maximum number of triangles 3160
Average clustering coefficient 0.963415
Fraction of closed triangles 0.963268
Maximum k-core 82
Lower bound of Maximum Clique 81
