Nodes 6691
Edges 19748
Maximum degree 12
Average degree 5
Assortativity -0.148239
Number of triangles 39294
Average number of triangles 5
Maximum number of triangles 13
Average clustering coefficient 0.434144
Fraction of closed triangles 0.383057
Maximum k-core 5
Lower bound of Maximum Clique 4
