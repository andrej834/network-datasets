Nodes 2597
Edges 74444
Maximum degree 122
Average degree 57
Assortativity -0.0674241
Number of triangles 2928764
Average number of triangles 1127
Maximum number of triangles 2832
Average clustering coefficient 0.802847
Fraction of closed triangles 0.538441
Maximum k-core 37
Lower bound of Maximum Clique 19
