Nodes 2000
Edges 11778
Maximum degree 210
Average degree 11
Assortativity -0.0502629
Number of triangles 38349
Average number of triangles 19
Maximum number of triangles 647
Average clustering coefficient 0.335964
Fraction of closed triangles 0.121267
Maximum k-core 7
Lower bound of Maximum Clique 7
