Nodes 39
Edges 170
Maximum degree 33
Average degree 8
Assortativity -0.375783
Number of triangles 582
Average number of triangles 14
Maximum number of triangles 71
Average clustering coefficient 0.450237
Fraction of closed triangles 0.28418
Maximum k-core 7
Lower bound of Maximum Clique 5
