Nodes 878
Edges 3285
Maximum degree 9
Average degree 7
Assortativity 0.366243
Number of triangles 9624
Average number of triangles 10
Maximum number of triangles 12
Average clustering coefficient 0.46022
Fraction of closed triangles 0.439272
Maximum k-core 5
Lower bound of Maximum Clique 4
