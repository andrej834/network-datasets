Nodes 183
Edges 886
Maximum degree 160
Average degree 9
Assortativity -0.384153
Number of triangles 5185
Average number of triangles 28
Maximum number of triangles 722
Average clustering coefficient 0.586696
Fraction of closed triangles 0.124562
Maximum k-core 11
Lower bound of Maximum Clique 8
