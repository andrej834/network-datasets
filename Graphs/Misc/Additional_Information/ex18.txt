Nodes 5772
Edges 66032
Maximum degree 34
Average degree 22
Assortativity 0.927217
Number of triangles 821496
Average number of triangles 142
Maximum number of triangles 288
Average clustering coefficient 0.440233
Fraction of closed triangles 0.479928
Maximum k-core 19
Lower bound of Maximum Clique 9
