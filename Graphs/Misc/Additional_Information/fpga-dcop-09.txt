Nodes 1220
Edges 4752
Maximum degree 70
Average degree 7
Assortativity -0.237134
Number of triangles 6896
Average number of triangles 5
Maximum number of triangles 68
Average clustering coefficient 0.246833
Fraction of closed triangles 0.0955906
Maximum k-core 6
Lower bound of Maximum Clique 3
