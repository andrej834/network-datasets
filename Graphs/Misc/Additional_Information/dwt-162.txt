Nodes 162
Edges 510
Maximum degree 8
Average degree 6
Assortativity 0.215441
Number of triangles 1392
Average number of triangles 8
Maximum number of triangles 12
Average clustering coefficient 0.506526
Fraction of closed triangles 0.472666
Maximum k-core 5
Lower bound of Maximum Clique 4
