Nodes 161
Edges 608
Maximum degree 16
Average degree 7
Assortativity -0.0838207
Number of triangles 1776
Average number of triangles 11
Maximum number of triangles 16
Average clustering coefficient 0.454185
Fraction of closed triangles 0.432749
Maximum k-core 6
Lower bound of Maximum Clique 4
