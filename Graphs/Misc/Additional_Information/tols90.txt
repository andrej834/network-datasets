Nodes 90
Edges 1674
Maximum degree 110
Average degree 37
Assortativity -0.690909
Number of triangles 68544
Average number of triangles 761
Maximum number of triangles 2516
Average clustering coefficient 1.59505
Fraction of closed triangles 0.570145
Maximum k-core 35
Lower bound of Maximum Clique 19
