Nodes 8140
Edges 1004381
Maximum degree 1525
Average degree 246
Assortativity -0.0254133
Number of triangles 160321356
Average number of triangles 19695
Maximum number of triangles 80181
Average clustering coefficient 0.440325
Fraction of closed triangles 0.238158
Maximum k-core 456
Lower bound of Maximum Clique 153
