Nodes 1090
Edges 2674
Maximum degree 110
Average degree 4
Assortativity 0.112167
Number of triangles 68544
Average number of triangles 62
Maximum number of triangles 2516
Average clustering coefficient 0.131701
Fraction of closed triangles 0.559899
Maximum k-core 35
Lower bound of Maximum Clique 19
