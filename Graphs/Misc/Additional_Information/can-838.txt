Nodes 838
Edges 4586
Maximum degree 31
Average degree 10
Assortativity -0.00998933
Number of triangles 24696
Average number of triangles 29
Maximum number of triangles 106
Average clustering coefficient 0.619657
Fraction of closed triangles 0.399379
Maximum k-core 8
Lower bound of Maximum Clique 7
