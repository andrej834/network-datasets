Nodes 600
Edges 13160
Maximum degree 54
Average degree 43
Assortativity 0.612796
Number of triangles 211680
Average number of triangles 352
Maximum number of triangles 444
Average clustering coefficient 0.381885
Fraction of closed triangles 0.367454
Maximum k-core 31
Lower bound of Maximum Clique 9
