Nodes 1960
Edges 4621
Maximum degree 540
Average degree 4
Assortativity -0.163216
Number of triangles 8199
Average number of triangles 4
Maximum number of triangles 762
Average clustering coefficient 0.590937
Fraction of closed triangles 0.0271045
Maximum k-core 4
Lower bound of Maximum Clique 5
