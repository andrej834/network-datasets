Nodes 62
Edges 140
Maximum degree 12
Average degree 4
Assortativity 0.191514
Number of triangles 84
Average number of triangles 1
Maximum number of triangles 6
Average clustering coefficient 0.166827
Fraction of closed triangles 0.139767
Maximum k-core 5
Lower bound of Maximum Clique 3
