Nodes 150
Edges 945
Maximum degree 14
Average degree 12
Assortativity 0.409219
Number of triangles 2388
Average number of triangles 15
Maximum number of triangles 19
Average clustering coefficient 0.216812
Fraction of closed triangles 0.21603
Maximum k-core 9
Lower bound of Maximum Clique 3
