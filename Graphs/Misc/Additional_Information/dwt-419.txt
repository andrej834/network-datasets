Nodes 419
Edges 1572
Maximum degree 12
Average degree 7
Assortativity 0.171089
Number of triangles 4590
Average number of triangles 10
Maximum number of triangles 21
Average clustering coefficient 0.456207
Fraction of closed triangles 0.422652
Maximum k-core 7
Lower bound of Maximum Clique 4
