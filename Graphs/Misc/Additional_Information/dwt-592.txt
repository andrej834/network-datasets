Nodes 592
Edges 2256
Maximum degree 14
Average degree 7
Assortativity 0.22105
Number of triangles 6720
Average number of triangles 11
Maximum number of triangles 27
Average clustering coefficient 0.460009
Fraction of closed triangles 0.427971
Maximum k-core 6
Lower bound of Maximum Clique 4
