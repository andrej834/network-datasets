Nodes 1270
Edges 3699
Maximum degree 6
Average degree 5
Assortativity 0.468835
Number of triangles 7290
Average number of triangles 5
Maximum number of triangles 6
Average clustering coefficient 0.409239
Fraction of closed triangles 0.403744
Maximum k-core 4
Lower bound of Maximum Clique 3
