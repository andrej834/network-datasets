Nodes 1000
Edges 9990
Maximum degree 37
Average degree 19
Assortativity 0.00250857
Number of triangles 4050
Average number of triangles 4
Maximum number of triangles 18
Average clustering coefficient 0.0201003
Fraction of closed triangles 0.0202882
Maximum k-core 15
Lower bound of Maximum Clique 4
