Nodes 1349
Edges 3876
Maximum degree 6
Average degree 5
Assortativity 0.46408
Number of triangles 7581
Average number of triangles 5
Maximum number of triangles 6
Average clustering coefficient 0.412824
Fraction of closed triangles 0.405379
Maximum k-core 4
Lower bound of Maximum Clique 3
