Nodes	3.4K
Edges	5
Density	8.80783e-07
Maximum degree	1
Minimum degree	0
Average degree	0
Assortativity	nan
Number of triangles	0
Average number of triangles	0
Maximum number of triangles	0
Average clustering coefficient	0
Fraction of closed triangles	nan
Maximum k-core	2
Lower bound of Maximum Clique	2


Category	Temporal Networks
Collection	Temporal Reachability Networks
Tags	Dynamic network, temporal reachability, temporal graph representation
Source	http://www.ryanrossi.com/papers/maxclique_tscc.pdf
Short	Retweet temporal reachability graph
Vertex type	User
Edge type	Temporal path via retweets
Format	Undirected
Edge weights	Unweighted
Description	In networks where edges represent a contact, a phone-call, an email, or physical proximity between two entities at a specific point in time, one gets an evolving network structure. 
		One useful way to investigate temporal networks is to transform the temporal graph (sequence of timestamped edges) into a (static) temporal reachability graph representing the possible
		flow of information/influence, etc. The temporal reachability graph is formed by placing an edge in the temporal reachability graph if there exists a "strong" temporal path between two vertices 
		(in both directions: from u to v, and from v to u). Hence, a temporal path represents a sequence of contacts that obeys time and therefore an edge in the temporal reachability graph represents the
		fact that a user could have transmitted a piece of information (or disease, etc) to that user (and vice-versa). This temporal graph representation is extremely useful for analyzing such networks and
		for planning applications. For instance, a temporal strong component is a set of vertices where all pairwise temporal paths exist.

[LOCATION]: http://networkrepository.com/scc-rt-lebanon.php