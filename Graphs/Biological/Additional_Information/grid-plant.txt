Nodes	1.7K
Edges	6.2K
Density	0.00420585
Maximum degree	142
Minimum degree	2
Average degree	7
Assortativity	0.0647757
Number of triangles	30K
Average number of triangles	17
Maximum number of triangles	1.3K
Average clustering coefficient	0.11914
Fraction of closed triangles	0.230253
Maximum k-core	25
Lower bound of Maximum Clique	9

Category	Sparse Networks
Collection	Biological Networks
Tags Bioinformatics
Format	Undirected

[LOCATION]: http://networkrepository.com/bio-grid-plant.php