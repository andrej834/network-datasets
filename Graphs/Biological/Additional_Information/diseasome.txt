Nodes	516
Edges	1.2K
Density	0.00894107
Maximum degree	50
Minimum degree	1
Average degree	4
Assortativity	0.0666456
Number of triangles	4.1K
Average number of triangles	7
Maximum number of triangles	152
Average clustering coefficient	0.63583
Fraction of closed triangles	0.430471
Maximum k-core	11
Lower bound of Maximum Clique	11

Category	Sparse networks
Collection	Biological networks
Tags Metabolic network, bioinformatics
Short	Metabolic network
Format	Undirected

[LOCATION]: http://networkrepository.com/bio-diseasome.php



