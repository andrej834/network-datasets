Nodes	400
Edges	71.8K
Density	0.9
Maximum degree	378
Minimum degree	333
Average degree	359
Assortativity	-0.0158223
Number of triangles	23.2M
Average number of triangles	57.9K
Maximum number of triangles	64.1K
Average clustering coefficient	0.90054
Fraction of closed triangles	0.900512
Maximum k-core	337
Lower bound of Maximum Clique	45