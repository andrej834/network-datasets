Nodes	1K
Edges	244.8K
Density	0.490088
Maximum degree	766
Minimum degree	232
Average degree	489
Assortativity	-0.0985585
Number of triangles	74M
Average number of triangles	74K
Maximum number of triangles	157.6K
Average clustering coefficient	0.58458
Fraction of closed triangles	0.568367
Maximum k-core	328
Lower bound of Maximum Clique	42