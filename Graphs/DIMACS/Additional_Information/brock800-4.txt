Nodes	800
Edges	207.6K
Density	0.649696
Maximum degree	565
Minimum degree	481
Average degree	519
Assortativity	-0.00383331
Number of triangles	69.9M
Average number of triangles	87.4K
Maximum number of triangles	103.3K
Average clustering coefficient	0.649685
Fraction of closed triangles	0.649681
Maximum k-core	486
Lower bound of Maximum Clique	19