Nodes	250
Edges	28K
Density	0.899084
Maximum degree	236
Minimum degree	203
Average degree	223
Assortativity	-0.00713806
Number of triangles	5.6M
Average number of triangles	22.4K
Maximum number of triangles	24.9K
Average clustering coefficient	0.899072
Fraction of closed triangles	0.899069
Maximum k-core	211
Lower bound of Maximum Clique	40