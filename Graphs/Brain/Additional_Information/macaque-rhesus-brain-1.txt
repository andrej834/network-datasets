Nodes	242
Edges	4.1K
Density	0.140256
Maximum degree	164
Minimum degree	2
Average degree	33
Assortativity	-0.0549821
Number of triangles	78.1K
Average number of triangles	322
Maximum number of triangles	2.7K
Average clustering coefficient	0.53256
Fraction of closed triangles	0.361515
Maximum k-core	26
Lower bound of Maximum Clique	8

Category	Sparse networks
Collection	Brain networks
Tags Brain, brain networks, Magnetic Resonance
Source	https://neurodata.io/data/
Short	Brain networks
Edge type	Fiber tracts
Edge weights	Unweighted
Description	Edges represent fiber tracts that connect one vertex to another.

[LOCATION]: http://networkrepository.com/bn-macaque-rhesus-brain-1.php